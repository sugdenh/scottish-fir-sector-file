SECTOR:AAG:0:750
OWNER:AAG:AAT:AAA:AAR:BEL:SCA:PXW:PX
BORDER:AAG
DEPAPT:EGAA
ARRAPT:EGAA

SECTOR:AAT:0:2000
OWNER:AAT:AAA:AAR:BEL:SCA:PXW:PX
BORDER:AAT
DEPAPT:EGAA

SECTOR:AAA:0:6000
OWNER:AAA:AAR:BEL:SCA:PXW:PX
BORDER:AAA
ARRAPT:EGAA

SECTOR:AAR:0:10000
OWNER:AAR:BEL:SCA:PXW:PX
BORDER:Antrim:EIDW_N_PXW:AAR_BEL:SCW_P600
GUEST:PX:*:EIDW
GUEST:PXW:*:EIDW
GUEST:DWN:*:EIDW
GUEST:AEA:*:EGAE
GUEST:PXW:*:EGAE
GUEST:PX:*:EGAE
ARRAPT:EGAA:EGAC
ARRAPT:EGAA:EGAC